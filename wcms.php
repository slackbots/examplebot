<?php
/*
When a matching chat message is received, a POST will be sent. The data is defined as follows:

token=Js8CvmHKtNfleVDnq4bU5f0Z
team_id=T0001
team_domain=example
channel_id=C2147483705
channel_name=test
timestamp=1355517523.000005
user_id=U2147483697
user_name=Steve
text=googlebot: What is the air-speed velocity of an unladen swallow?
trigger_word=googlebot:

Text must be returned in JSON format. Formatting help: https://api.slack.com/docs/formatting
*/

$token = 'Js8CvmHKtNfleVDnq4bU5f0Z'; //token from the Outgoing Webhooks setup page, so we don't respond to other requests
$response = 'Hi.'; //default response

if (!isset($_POST['token']) || $_POST['token'] != $token) {
  $response = 'unknown token';
} else {
  //figure out the user
  $user = '<@' . $_POST['user_id'] . '|' . $_POST['user_name'] . '>';
  //figure out the request and clean it up
  $request = substr($_POST['text'], strlen($_POST['trigger_word']));
  $request = trim($request, ",.:;!? \t\n\r\0\x0B");
  $lower_request = strtolower($request);

  //parse request and respond
  if ($request == '') {
    $response = 'Hi there ' . $user . '!';
  }
  elseif ($lower_request == 'help') {
    $response = "Here's what I can do right now:\n";
    $response .= "• say hello (just say my name)\n";
    $response .= "• show a random goose image (goose, goose me, gimme a goose, give me a goose)\n";
    $response .= "• answer yes or no/yea or nay (yes or no, yea or nay)\n";
    $response .= "• give a random number (give me a random number, give me a random number between [x] and [y], random, random [x] [y])\n";
  }
  elseif (in_array($lower_request, array('goose', 'goose me', 'gimme a goose', 'give me a goose'))) {
    $response = 'Here you go!';
    $attachment['fallback'] = "Random goose from http://placegeese.com"; //fallback is required for this to work
    $attachment['image_url'] = 'http://placegeese.com/200/200.jpg';
    $attachments[] = $attachment;
  }
  elseif ($lower_request == 'yea or nay' || $lower_request == 'yes or no') {
    $yea = rand(0, 1);
    if ($yea) {
      $response = 'I say ' . substr($lower_request, 0, 3) . '. :thumbsup:';
    } else {
      $response = 'Gonna have to say ' . substr($lower_request, 7) . '. :thumbsdown:';
    }
  }
  elseif (substr($lower_request, 0, strlen('give me a random number')) == 'give me a random number' || substr($lower_request, 0, strlen('random')) == 'random') {
    //check for additional info
    $request = strtolower($request);
    if (preg_match('/^give me a random number between (\d+) (?:&|and) (\d+)$/', $request, $matches) || preg_match('/^random (\d+) (\d+)$/', $request, $matches)) {
      $response = random($matches[1], $matches[2]);
    }
    elseif ($lower_request == 'give me a random number' || $lower_request == 'random') {
      $response = random();
    }
    else {
      $response = 'I\'m sorry, ' . $user . ', that\'s a random thing I don\'t know how to do.';
    }
  }
  else {
    $response = 'I\'m sorry, ' . $user . ', I don\'t understand "' . $request .'".';
  }
}

function random($low = NULL, $high = NULL) {
  if ($low === NULL || $high === NULL) {
    $random = rand();
  }
  else {
    $random = rand($low, $high);
  }
  return 'Your random number is *' . $random . '*.';
}
$full_response['text'] = $response;
if (isset($attachments)) {
  $full_response['attachments'] = $attachments;
}
//echo '<pre>';
echo json_encode($full_response);
//echo '</pre>';
?>
